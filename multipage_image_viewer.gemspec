# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'multipage_image_viewer/version'

Gem::Specification.new do |spec|
  spec.name          = "multipage_image_viewer"
  spec.version       = MultipageImageViewer::VERSION
  spec.authors       = ["NLI", "Lutz Biedinger"]
  spec.email         = ["ithelpdesk@nli.ie"]
  spec.summary       = "A gem to provide the multipage image viewer javascript libraries and stylesheets."
  spec.description   = "This is a gem to provide the multipage image viewer functionality, which is derived from the open book reader."
  spec.homepage      = "https://bitbucket.org/nlireland/multipage_image_viewer"
  spec.license       = "AGPL version 3"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
end
