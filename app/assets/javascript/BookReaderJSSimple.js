if (typeof multipage_id !== 'undefined'){

    var topPadding = 50;
    var padding = 10;
    var sidePadding = 20;
    var pages;
    var maxWidth = [];
    var maxHeight = [];
    var totalHeight = [];
    var firstAjaxRequest = true;
    var maxZoom;
    var zoomLimit = 4;
    var zoomLevel = 1;//br.getZoomLevel();
    var fileName = "";
    var ajaxRequest; //this function is declared below

    br = new BookReader();

    fileName = multipage_id.replace("vtls","");

    //Perform the AJAX request; afterwards, execute the callback
    ajaxRequest = function() {
        $.ajax({
            url: '/diva/php/divaserve.php?d=' + fileName,
            cache: true,
            context: this, // Not sure if necessary
            async: false,
            dataType: 'json',
            success: function(data) {

                // Update the pages data
                pages = data.pgs;

                // If it's the first request, set some initial settings
                if (firstAjaxRequest) {
                    // Total number of leafs
                    br.numLeafs = pages.length;
                }
                // Clear the document
                maxWidth = data.dims.max_w;
                maxHeight = data.dims.max_h;
                totalHeight = data.dims.t_hei;
                maxZoom = data.max_zoom;
                firstAjaxRequest = false;
            }
        });
    };

    ajaxRequest(zoomLevel);

    // Return the width of a given page.  Here we assume all images are 800 pixels wide
    br.getPageWidth = function(index) {
    	var pageIndex = index;
    	var pWidth = pages[pageIndex].d[self.zoomLevel].w;
        return (pWidth > maxWidth[self.zoomLevel] ? maxWidth[self.zoomLevel] : pWidth);
    }

    // Return the height of a given page.  Here we assume all images are 1200 pixels high
    br.getPageHeight = function(index) {
    	var pageIndex = index;
    	var pHeight = pages[pageIndex].d[self.zoomLevel].h;
        return (pHeight > maxHeight[self.zoomLevel] ? maxHeight[self.zoomLevel] : pHeight);
    }

    br.getPageMaxZoom = function(index) {
    	var pageIndex = index;
    	var pMaxZoom = pages[pageIndex].m;
        return pMaxZoom;
    }

    // We load the images from archive.org -- you can modify this function to retrieve images
    // using a different URL structure
    br.getPageURI = function(index, reduce, rotate) {
        // reduce and rotate are ignored in this simple implementation, but we
        // could e.g. look at reduce and load images from a different directory
        // or pass the information to an image server
        var id = ('000' + (index+1)).slice(-3);
        var serverFileName = pages[index]["f"];
        var leafStr;
        if (serverFileName == "restricted.jp2"){
            leafStr = serverFileName;
        } else{
            leafStr = getParentDir(fileName, 10000)+ '/' + fileName + '/' + serverFileName;
        }
        var url = IMAGE_SERVER_URL +'?FIF='+ leafStr;// [path to IIPServer fcgi file]?FIF=/jp2s/Tifs/napoleonofnottin00chesrich_0001.tif&CVT=JPG';
        return url;
    }

    function getParentDir(number, to) {
    	var dirOut = number - number%to + to;
        return ("000000000" + dirOut).slice (-9);
    }

    // Return which side, left or right, that a given page should be displayed on
    br.getPageSide = function(index) {
        if (0 == (index & 0x1)) {
            return 'R';
        } else {
            return 'L';
        }
    }

    // This function returns the left and right indices for the user-visible
    // spread that contains the given index.  The return values may be
    // null if there is no facing page or the index is invalid.
    br.getSpreadIndices = function(pindex) {
        var spreadIndices = [null, null];
        if ('rl' == this.pageProgression) {
            // Right to Left
            if (this.getPageSide(pindex) == 'R') {
                spreadIndices[1] = pindex;
                spreadIndices[0] = pindex + 1;
            } else {
                // Given index was LHS
                spreadIndices[0] = pindex;
                spreadIndices[1] = pindex - 1;
            }
        } else {
            // Left to right
            if (this.getPageSide(pindex) == 'L') {
                spreadIndices[0] = pindex;
                spreadIndices[1] = pindex + 1;
            } else {
                // Given index was RHS
                spreadIndices[1] = pindex;
                spreadIndices[0] = pindex - 1;
            }
        }

        return spreadIndices;
    }

    // For a given "accessible page index" return the page number in the book.
    //
    // For example, index 5 might correspond to "Page 1" if there is front matter such
    // as a title page and table of contents.
    br.getPageNum = function(index) {
        return index+1;
    }

    br.initUIStrings = function(){
        // Navigation handlers will be bound after all UI is in place -- makes moving icons between
        // the toolbar and nav bar easier
        // Setup tooltips -- later we could load these from a file for i18n
        var titles;
        if (locale == "en"){
            titles = { '.zoom_in': 'Zoom in',
                           '.zoom_out': 'Zoom out',
                           '.onepg': 'One-page view',
                           '.thumb': 'Thumbnail view',
                           '.print': 'Print this page',
                           '.full': 'Show fullscreen',
                           '.reset': 'Reset Image',
                           '.inverse': 'Inverse Image',
                           '.contrast_plus': 'Increase Contrast',
                           '.contrast_minus': 'Decrease Contrast',
                           '.brightness_plus': 'Increase Brightness',
                           '.brightness_minus': 'Decrease Brightness',
                           '.search-within': 'Filter Events/Dates',
                           '.download': 'Download',
                           '.xsBRicons': 'View Controls'
                          };
            if ('rl' == this.pageProgression) {
                titles['.book_leftmost'] = 'Last page';
                titles['.book_rightmost'] = 'First page';
            } else { // LTR
                titles['.book_leftmost'] = 'First page';
                titles['.book_rightmost'] = 'Last page';
            }
        } else if (locale == "ga"){
            titles = { '.zoom_in': 'Súmáil isteach',
                           '.zoom_out': 'Súmáil amach',
                           '.onepg': 'Amharc aon leathanaigh',
                           '.thumb': 'Amharc mionsamhlacha',
                           '.print': 'Priontáil an leathanach seo',
                           '.full': 'Taispeáin lánscáileán',
                           '.reset': 'Athshocraigh Íomhá',
                           '.inverse': 'Inbhéartaigh Íomhá',
                           '.contrast_plus': 'Méadaigh Codarsnacht',
                           '.contrast_minus': 'Laghdaigh Codarsnacht',
                           '.brightness_plus': 'Méadaigh Gile',
                           '.brightness_minus': 'Laghdaigh Gile',
                           '.search-within': 'Scag Imeachtaí/Dátaí',
                           '.download': 'Íoslódáil',
                           '.xsBRicons': 'Amharc ar Rialtáin'
                          };
            if ('rl' == this.pageProgression) {
                titles['.book_leftmost'] = 'Last pageG';
                titles['.book_rightmost'] = 'First pageG';
            } else { // LTR
                titles['.book_leftmost'] = 'First pageG';
                titles['.book_rightmost'] = 'Last pageG';
            }
        }

        for (var icon in titles) {
            if (titles.hasOwnProperty(icon)) {
                $('#BookReader').find(icon).attr('title', titles[icon]);
            }
        }
    }

    // Book title and the URL used for the book title link
    br.bookTitle= 'sd';
    br.bookUrl  = '#';

    // Override the path used to find UI images
    br.imagesBaseURL = '../BookReader/images/';

    br.getEmbedCode = function(frameWidth, frameHeight, viewParams) {
        return "Embed code not supported.";
    }
    $(function() {
        // Let's go!
        br.init();

        $('#BRtoolbar').find('.read').hide();
        $('#textSrch').hide();
        $('#btnSrch').hide();

        //CODE TO SHOW INFO DIV OVER VIEWER ON RIGHTCLICK
        var infoDiv = $('<div id="infoDiv"><div><a href="http://www.nli.ie" target="blank">National Library of Ireland</a><ul>' +
        		'<li>Use the +/- buttons in the top right of the viewer to zoom in/out.</li>'+
        		'<li>Switch between page/overview views using the respective buttons in the top right.</li>'+
        		'<li>You can jump to a page by using the page slider at the top of the viewer.</li>'+
        		'</ul><div class="BRInfoExternalLinks">using <a href="http://openlibrary.org/dev/docs/bookreader" target="blank">Internet Archive BookReader</a> and <a href="http://iipimage.sourceforge.net/" target="blank">IIPImage Server</a></div></div></div>');
        //ie9 is being funny with all the iframe inclusion and doesn't import the IE8 stylesheet so it doesn't handle the transparent background properly
        //if($.browser.msie){
        //	infoDiv.css("background-color", "#000");
        //}

        infoDiv.click(function(){
        	infoDiv.fadeOut();
        });
        function showInfoDiv(){
        	infoDiv.fadeIn();
        }


        $("#BookReader").append(infoDiv);
        $("#BookReader").bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });
}


